Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#dashboard'

  resources :home, only: [] do
    collection do
      get :stream_by_hashtag
      get :stream_by_user
    end
  end

  resources :analytics, only: [] do
    collection do
      get :stream_by_hashtag
      get :dashboard
    end
  end
end
