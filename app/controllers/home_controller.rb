class HomeController < ApplicationController
  before_action :set_client, only: [:stream_by_hashtag, :stream_by_user]

  def dashboard
  end

  def stream_by_hashtag
    @topic = params[:hashtag]
    render layout: false
  end

  def stream_by_user
    @user = params[:username]
    render layout: false
  end

  private

  def set_client
    @client = TwitterClient.client_object
  end
end