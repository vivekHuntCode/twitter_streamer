$(function(){
  App.tweet_incoming = App.cable.subscriptions.create({
    channel: 'TweetByHashIncomingChannel'
    }, {
    received: function(data) {
      $('.card #by_hashtag').prepend('<div class="row"><div class="col-12"><div class="alert alert-success" role="alert">' + data.message_id + '</div></div></div>');
    }
  });
​
  App.tweet_incoming = App.cable.subscriptions.create({
    channel: 'TweetByUserIncomingChannel'
    }, {
    received: function(data) {
      $('.card #by_user').prepend('<div class="row"><div class="col-12"><div class="alert alert-success" role="alert">' + data.message_id + '</div></div></div>');
    }
  });
​
  $('form#form_by_hashtag').on('submit', function(e){
    e.preventDefault();
    $('.tweets_container#by_hashtag').empty();
    $.get('/home/stream_by_hashtag', { hashtag: $('form #hashtag_input').val()}, {}, 'script');
  })
​
  $('form#form_by_user').on('submit', function(e){
    e.preventDefault();
    $('.tweets_container#by_user').empty();
    $.get('/home/stream_by_user', { username: $('form #user_input').val()}, {}, 'script');
  })
});