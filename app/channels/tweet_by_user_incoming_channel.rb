class TweetByUserIncomingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "tweet_by_user_incoming_channel"
  end
end
