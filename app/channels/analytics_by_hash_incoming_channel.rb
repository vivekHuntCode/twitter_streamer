class AnalyticsByHashIncomingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "analytics_by_hash_incoming_channel"
  end
end
