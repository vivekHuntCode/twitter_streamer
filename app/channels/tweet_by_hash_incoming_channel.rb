class TweetByHashIncomingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "tweet_by_hash_incoming_channel"
  end
end
