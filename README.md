# README

This rails application streams and monitors tweets from a user or by a hash tag

To get started:

```bash
bundle install
rake db:create
rails s
```
Also, update the following credentials in credentials file by running ```rails credentials:edit```:
```yaml
consumer_key: < YOUR consumer_key >
consumer_secret: < YOUR consumer_secret >
access_token: < YOUR access_token >
access_token_secret: < YOUR access_token_secret >
```
The dashboard has two input options, one to stream tweets by hash tags, and the other for tweets by user name.